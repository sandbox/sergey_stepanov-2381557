/**
 * @file
 * Xmpp support chat messenger.
 */

(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.xmpp_support_chat = {
    attach: function(context, settings) {
      t_ch(0);
      var chat_mess_status = 'stopped';
      var xmppmess = '';
      var freq = $.cookie('Drupal.visitor.xmpp_support_chat_freq');

      if (!supports_html5_storage()){
        $('#slide_xmpp_support_chat_main').hide();
        chat_mess_status = 'hide';
      } else {
        if (!localStorage["xmpp_support_chat_cursor"]) {
          // Set otions for loacl storage.
          localStorage["xmpp_support_chat_cursor"] = 1;
          localStorage["xmpp_support_chat_maxcount"] = 50;
          if (!localStorage["lastRequest"]) {localStorage["lastRequest"] = 0;}
        }
        maxMessCount = parseInt(localStorage["xmpp_support_chat_maxcount"]);
        applayNewMessage(false);
        if ($.cookie('xmpp_support_chat_message_status') == 'chat'){
          checkMessages();
        }
      }

      // Click to header of chat area.
      $('#slide_xmpp_support_chat_header').click(function(event) {
        if (parseInt($('#slide_xmpp_support_chat_main').css('height')) > 100) {
          $('#slide_xmpp_support_chat_main').animate({ height: 20, width: 150 }, 700);
          $('#slide_xmpp_support_chat_header').css('cursor', 'n-resize');
        } else{
          $('#slide_xmpp_support_chat_main').animate({ height: 260, width: 450 }, 1000);
          $('#slide_xmpp_support_chat_header').css('cursor', 's-resize');
          if ($.cookie('xmpp_support_chat_message_status') != 'chat'){
            chat_mess_status = 'stopped';
            checkMessages();
          }
        }
      })

      // Static variable - time mark of last message.
      function t_ch(value) {
        if (!t_ch.time) {
          t_ch.time = 0
        };
        if (value != null){
          if (value > t_ch.time){
            t_ch.time = value;
          }
        }
        return t_ch.time;
      }

      // Sending message.
      $('#xmpp_support_chat_message_form').submit(function(e) {
        // Fix for IE.
        e = $.event.fix(e);
        xmppmess = xmppmess + $("#xmpp_support_chat_message").val();
        $("#xmpp_support_chat_message").val("");
        if ((!$.cookie('xmpp_support_chat_message_status')) || ($.cookie('xmpp_support_chat_message_status') != 'chat')){
          checkMessages();
        }
        e.preventDefault();
      });

      function refreshMessages(){
        $.ajax({
          url: "/xmpp_support_chat/refresh",
          type: "POST",
          data: {change: t_ch()},
          success: onSucc,
          complete: onCompl,
          dataType: 'text'
        });
        chat_mess_status = 'started';
      };

      function applayNewMessage(show){
        mess = chatLocalLoad(t_ch());
        if (mess){
          for(var i in mess) {
            if (!mess.hasOwnProperty(i)){
              continue;
            }
            $("#xmpp_support_chat_textarea").append(mess[i]);
            if ((parseInt($('#slide_xmpp_support_chat_main').css('height')) < 100) && show) {
              $('#slide_xmpp_support_chat_main').animate({ height: 260, width: 450 }, 1000);
              $('#slide_xmpp_support_chat_header').css('cursor', 's-resize');
            }
            if (i > 0) {t_ch(i);}
          }
          if (mess.length > 0){
            $("#xmpp_support_chat_textarea").animate({
              scrollTop: document.getElementById("xmpp_support_chat_textarea").scrollHeight - $("#xmpp_support_chat_textarea").height()
            }, 300);
          }
        }
      }

      function checkMessages() {
        d = new Date();
        if (chat_mess_status == 'stopped'){
          if (((d.getTime() - parseInt(localStorage["lastRequest"]) > freq) && ($.cookie('xmpp_support_chat_message_sleep') != 3)) || (xmppmess.length > 0)){
            if (chat_mess_status != 'hide') {chat_mess_status = 'started';}
            localStorage["lastRequest"] = d.getTime();
            $.ajax({
              url: "/xmpp_support_chat/request",
              type: "POST",
              data: { mess: xmppmess , name: xmpp_support_chat_user.value, change: t_ch},
              success: onSucc,
              complete: onCompl,
              dataType: 'text'
            });
            xmppmess = '';
            return true;
          } else {
            applayNewMessage(true);
          }
        }
        setTimeout(checkMessages, freq * 1.1);
        return false;
      }

      function onCompl(jqXHR, textStatus){
        if (chat_mess_status != 'hide') {chat_mess_status = 'stopped';}
        setTimeout(checkMessages, freq);
      }

      function onSucc(html) {
        var mess = $.parseJSON(html);
        if (mess != null){
          for(var i in mess) {
            if (!mess.hasOwnProperty(i)){
              continue;
            }
            $("#xmpp_support_chat_textarea").append(mess[i]);
            SaveChatMessage(mess[i], i);
            if (i > 0) {
              t_ch(i);
            }
            if (parseInt($('#slide_xmpp_support_chat_main').css('height')) < 100) {
              $('#slide_xmpp_support_chat_main').animate({ height: 260, width: 450 }, 1000);
              $('#slide_xmpp_support_chat_header').css('cursor', 's-resize');
            }
          }
          $("#xmpp_support_chat_textarea").animate({
              scrollTop: document.getElementById("xmpp_support_chat_textarea").scrollHeight - $("#xmpp_support_chat_textarea").height()
          }, 300);
        }
      }/*function onSucc*/

      function supports_html5_storage() {
        try {
          return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
          return false;
        }
      }

      function chatNextIndex(){
        cur = parseInt(localStorage["xmpp_support_chat_cursor"]);
        if (cur >= maxMessCount) {
          cur = 1;
        } else {
          cur++;
        }
        localStorage["xmpp_support_chat_cursor"] = cur;
        return cur;
      }

      // Load chat history from local storage.
      function chatLocalLoad(tt){
        var rez = new Array();
        d = new Date();
        if (!tt) {tt = 0};
        // Load part after cursor.
        for(var i = (parseInt(localStorage["xmpp_support_chat_cursor"]) + 1); i <= maxMessCount; i++){
          if (localStorage['chatMessage' + i]){
            temp = JSON.parse(localStorage['chatMessage' + i]);
            // Remove old message.
            if (temp.t < (d.getTime() / 1000 - 18000)){
              localStorage.removeItem('chatMessage' + i);
            }
            else {
              if (temp.t > tt) {rez[temp.t] = temp.mess;};
            }
          }
        }

        // Load part before cursor.
        for(var i = 1; i <= localStorage["xmpp_support_chat_cursor"]; i++){
          if (localStorage['chatMessage' + i]){
            temp = JSON.parse(localStorage['chatMessage' + i]);
            // Remove old message.
            if (temp.t < (d.getTime() / 1000 - 18000)){
              localStorage.removeItem('chatMessage' + i);
            }
            else {
              if (temp.t > tt) {rez[temp.t] = temp.mess;};
            }
          }
        }
        return rez;
      }

      function chatMessageExist(t){
        return chatLocalLoad()[t];
      }

      function SaveChatMessage(_mess_, i){
        if (chatMessageExist(i)){
          return false;
        }
        var data = new Object;
        data['mess'] = _mess_;
        data['t'] = i;
        localStorage['chatMessage' + chatNextIndex()] = JSON.stringify(data);
        return true;
      };
    }
  };

})(jQuery, Drupal, this, this.document);
