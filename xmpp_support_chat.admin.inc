<?php

/**
 * @file
 * Options form for xmpp support chat.
 */

/**
 * Make form for xmpp support chat.
 */
function xmpp_support_chat_config_form() {
  $form = array();
  $form['xmpp_support_chat_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Address of XMPP server'),
    '#default_value' => variable_get('xmpp_support_chat_server', '127.0.0.1'),
    '#required' => TRUE,
  );

  $form['xmpp_support_chat_server_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain of XMPP server'),
    '#default_value' => variable_get('xmpp_support_chat_server_domain', 'example.org'),
    '#description' => t('Second part of the users name for your XMPP server'),
    '#required' => TRUE,
  );

  $form['xmpp_support_chat_send_to'] = array(
    '#type' => 'textfield',
    '#title' => t('XMPP Account of reciver'),
    '#default_value' => variable_get('xmpp_support_chat_send_to', 'admin'),
    '#description' => t('Name of user on XMPP server who will receive messages from chat'),
    '#required' => TRUE,
  );

  $form['xmpp_support_chat_from'] = array(
    '#type' => 'textfield',
    '#title' => t('XMPP Account of sender'),
    '#default_value' => variable_get('xmpp_support_chat_from', 'admin'),
    '#description' => t('Name of user on XMPP server who will send special requests to the XMPP server'),
    '#required' => TRUE,
  );

  $form['xmpp_support_chat_from_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('password for XMPP Account of sender'),
    '#default_value' => variable_get('xmpp_support_chat_from_pass', 'pass'),
    '#required' => TRUE,
  );

  $form['xmpp_support_chat_request_frequency'] = array(
    '#type' => 'textfield',
    '#title' => t('Request Frequency'),
    '#default_value' => variable_get('xmpp_support_chat_request_frequency', 3000),
    '#description' => t('Number milliseconds between between requsts from pages to server (all pages in one visiter have one timer)'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );

  $form['xmpp_support_chat_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for chat area'),
    '#default_value' => variable_get('xmpp_support_chat_title', 'Live chat'),
    '#description' => t('This writing will show in the header of the chat'),
    '#required' => TRUE,
  );

  $form['xmpp_support_chat_exclude_show'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude show chat area from pages:'),
    '#default_value' => variable_get('xmpp_support_chat_exclude_show', 'admin/*'),
    '#description' => t('Exclude show chat area from pages'),
    '#required' => TRUE,
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'xmpp_support_chat_config_form_submit';

  return $form;
}

/**
 * Crete request to operator 'add me to your contact list'.
 */
function xmpp_support_chat_config_form_submit() {
  $messenger = new XmppSupportChatMessenger(
    array(
      'jid' => variable_get('xmpp_support_chat_from', 'admin') . '@' .
      variable_get('xmpp_support_chat_server_domain', 'mysite'),
      'pass' => variable_get('xmpp_support_chat_from_pass', 'admin'),
      'host' => variable_get('xmpp_support_chat_server', '127.0.0.1'),
      'protocol' => 'tcp',
      'auth_type' => 'PLAIN',
      'strict' => FALSE,
      'port' => 5222,
      'type' => 'chat',
    ));
  $to = variable_get('xmpp_support_chat_send_to', 'admin') . '@' .
        variable_get('xmpp_support_chat_server_domain', 'example.org');
  if ($messenger->askSubscribe($to)) {
    drupal_set_message(
      t("Request for subscribe sent to XMPP user @name",
        array('@name' => variable_get('xmpp_support_chat_send_to', 'admin'))
      ),
      'status'
    );
  }
  else {
    drupal_set_message(
      t("Cannot send request by XMPP user @name",
        array('@name' => variable_get('xmpp_support_chat_send_from', 'admin'))
      ),
      'error'
    );
  }
}
