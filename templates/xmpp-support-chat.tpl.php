<?php
/**
 * @file
 * Template for chat area.
 */
?>
<div id="slide_xmpp_support_chat_main">
  <div id="slide_xmpp_support_chat_header"><?php print check_plain(variable_get('xmpp_support_chat_title', 'Live chat')) ?></div>
  <div id="slide_xmpp_support_chat">
    <?php print t('Set name') ?>: <input id="xmpp_support_chat_user" type=text>
    <div id="xmpp_support_chat_textarea"></div>
    <form id="xmpp_support_chat_message_form" action"" method=post>
      <?php print t('Message') . ':<input id="xmpp_support_chat_message" autocomplete="off" type=text><input type=submit value="' . t('Send') . '">'?>

  </div>
</div>
