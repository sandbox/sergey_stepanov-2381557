<?php
/**
 * @file
 * Template for customer messages.
 *
 * Params:
 * $present_name - name from customer,
 * $message - message text,
 * $to_name - loggin of operator,
 * $domain - domain of server,
 * $name - loggin of customer,
 * $show_name - name sended to operator.
 */
?>
<div class="xmpp-support-chat-customer-row">
  <span class="xmpp-support-chat-customer-name">  <?php print htmlentities($show_name) ?> </span> :
  <span class="xmpp-support-chat-customer-message"> <?php print htmlentities($message)  ?></span><br/>
</div>
