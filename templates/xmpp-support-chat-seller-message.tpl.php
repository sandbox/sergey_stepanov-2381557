<?php
/**
 * @file
 * Template for seller messages.
 *
 * Params:
 * $to_name - name part of user loggin,
 * $to_domain - domain of xmpp server,
 * $to_resource - subject of operator,
 * $from_name - name of operator,
 * $from_resource - subject from operator.
 */
?>
<div class="xmpp-support-chat-seller-row">
  <span class="xmpp-support-chat-seller-name">  <?php print htmlentities($from_name) ?> </span> :
  <span class="xmpp-support-chat-seller-message"> <?php print ($message)  ?></span><br/>
</div>
