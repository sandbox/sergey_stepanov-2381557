CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers



DESCRIPTION
------------
This module creates a "chat" area on a web page for fast communication between
people using the site and sepecific "operators" (such as for a "live support"
chat). Each operator uses an XMPP client (such as Jabber). The module does not
implement an XMPP server: a list is available at XMPP Servers:
http://xmpp.org/xmpp-software/servers/

REQUIREMENTS
------------
 - This module requires external XMPP server (tested with eJabber).
 - The Libraries API module
 - 3rd party library: JAXL
 - A "content" region in your theme (most themes have this by default).


INSTALLATION
------------
This module does not implement XMPP server, so you must install it. The module
does not use any specific features of XMPP servers, so you can use any XMPP 
server:

 - ejabber
 - openfire
 - jabberd2

You can install it to any VDS hosting example: digitalocean (It is the cheapest
hoster for it, what I found).nstallation command for ejabber in debian:

apt-get install ejabber.

1.Download this module and copy it to your site, for example using
  "drush pm-download xmpp_support" or otherwise. See: 
  https://drupal.org/documentation/install/modules-themes/modules-7 for further
  information.
2.Download JAXL v3.0.0, the php XMPP and HTTP client/server library, from gitHub
  https://github.com/jaxl/JAXL/releases and instal it to folder
  site/all/libraries/JAXL.
3.Create directory site/all/libraries/JAXL/.jaxl for temp files and set
  permission for writing to that
4.Install this module as you would normally install a contributed Drupal module.


CONFIGURATION
-------------
 * Go to setup page (yourSite/admin/config/services/xmpp_support_chat_config) 
   and set options for XMPP server and two accounts:
    - first for operator of messenger.
    - second for service requests from web client.
 * After submit options first account must recive request for subscribe,
   you must allow it through you XMPP client".
 * You can set permission for roles who will see chat area.

TROUBLESHOOTING
---------------
 * If the chat area does not display, check the following:
   - User role have permission for XMPP support chat.
   - Account of operator authorized (have in contact list) the service account.
     Service account send request when you save configuration in configuration 
     page.
   - Check status of operator account it's need be online.
 * if you are using ejabber server - check:
   - your "sender user" is admin for ejabber - example if your user name is "admin"

       admin:
          user:
              - "admin": "localhost"
   - set register timeout:
          registration_timeout: infinity


MAINTAINERS
-----------
Current maintainer:
* Sergey S. (serg_admin) - https://www.drupal.org/user/3101115
