<?php

/**
 * @file
 * Send and receive messages.
 */

/**
 * Interface for xmpp protocol.
 */
class XmppSupportChatMessenger {
  private $xmppClient;
  private $params = array();
  private $respons = '';
  private $resour;
  private $result;
  private $msg;
  private $opStatus = TRUE;
  private $auth = TRUE;
  private $rawData;
  private $username;

  /**
   * Get conect status of client.
   *
   * @return bool
   *   result of authentification process.
   */
  public function connected() {
    return $this->auth;
  }

  /**
   * Wait list of registration fields from server.
   *
   * @param string $event
   *   type of event for JAXL library.
   * @param JAXLXml[] $args
   *   Array of incoming data from XMPP server. More see to
   *   http://xmpp.org/rfcs/rfc3920.html#stanzas.
   *
   * @return string
   *   "logged_out - event will disable else event stay enabled.
   */
  public function waitForRegisterForm($event, array $args) {
    $stanza = $args[0];
    $query = $stanza->exists('query', NS_INBAND_REGISTER);

    if ($query) {
      $form = array(
        'username' => $this->username,
        'password' => variable_get('xmpp_support_chat_from_pass'),
      );
      $this->xmppClient->xeps['0077']->set_form(
        $stanza->attrs['from'],
        $form
      );
      return array($this, 'waitForRegisterResponse');
    }
    else {
      $this->xmppClient->end_stream();
      return "logged_out";
    }
  }

  /**
   * Event for response about registration user.
   *
   * @param string $event
   *   type of event for JAXL library.
   * @param JAXLXml[] $args
   *   Array of incoming data from XMPP server. More see to
   *   http://xmpp.org/rfcs/rfc3920.html#stanzas.
   *
   * @return string
   *   "logged_out - event will disable else event stay enabled.
   */
  public function waitForRegisterResponse($event, array $args) {
    if ($event == 'stanza_cb') {
      $stanza = $args[0];
      if ($stanza->name == 'iq') {
        if ($stanza->attrs['type'] == 'result') {
          $this->xmppClient->send_end_stream();
          return "logged_out";
        }
        elseif ($stanza->attrs['type'] == 'error') {
          $error = $stanza->exists('error');
          print (t("registration failed with error code: @code and type: @type @peol",
            array(
              '@code' => $error->attrs['code'],
              '@type' => $error->attrs['type'],
              '@peol' => PHP_EOL,
            )
          ));
          print (t("error text: @err.@eo", array(
              '@err' => $error->exists('text'),
              '@eo' => PHP_EOL,
            )
          ));
          print (t("shutting down...")) . PHP_EOL;
          $this->xmppClient->send_end_stream();
          return "logged_out";
        }
      }
    }
    else {
      _notice("unhandled event $event rcvd");
    }
    return "logged_out";
  }

  /**
   * Event like onConnect.
   *
   * @param JAXLXml $stanza
   *   Incomming data from XMPP server. More see to
   *   http://xmpp.org/rfcs/rfc3920.html#stanzas.
   *
   * @return string
   *   "logging out - event will disable".
   *   "logging in - event stay enabled".
   */
  public function clientOnStreamFeatures(JAXLXml $stanza) {
    $this->xmppClient->xeps['0077']->get_form(
      variable_get('xmpp_support_chat_server_domain', 'example.org')
    );
    return array($this, 'waitForRegisterForm');
  }

  /**
   * Function for create new user in XMPP server.
   *
   * @param string $username
   *   Name of new user.
   *
   * @return string
   *   Name of new user.
   */
  public function registerUser($username) {
    libraries_load("JAXL");
    $this->username = $username;
    $this->xmppClient->require_xep(array('0077'));
    $this->form = array();

    $this->xmppClient->add_cb(
      'on_stream_features',
      array($this, 'clientOnStreamFeatures')
    );

    $this->xmppClient->start();
    return $this->username;
  }

  /**
   * Generate XML for present stanza.
   *
   * @param string $to
   *   Account which will receives query.
   * @param string $from
   *   Account sender this request.
   * @param string $type
   *   Type of request:
   *   probe - get status,
   *   subscribe - add account "from" to contact list.
   *
   * @return string
   *   XML present stanza.
   */
  private function getPresentStanza($to, $from, $type) {
    return "<presence type=\"$type\" to=\"$to\" from=\"$from\"/>";
  }

  /**
   * Make message to operator for subscribe.
   *
   * @return string
   *   TRUE - if OK.
   *   FALSE - if wrong requst.
   */
  public function askSubscribe($to) {
    $this->rawData = $this->getPresentStanza($to, $this->params['jid'], 'subscribe');
    $this->xmppClient->add_cb('on_auth_failure', array($this, 'funOnAuthFailure'));
    $this->xmppClient->add_cb('on_auth_success', array($this, 'funOnAuthSuccessSendRawData'));
    $this->xmppClient->start();

    return $this->auth;
  }

  /**
   * Event for interrupt XMPP client after timeout.
   *
   * @param string $to
   *   Account's name which status we are getting.
   *
   * @return string
   *   TRUE - if on line,
   *   FALSE - if off line.
   */
  public function getStatus($to) {
    $this->rawData = $this->getPresentStanza($to, $this->params['jid'], 'probe');
    $this->opStatus = FALSE;
    user_cookie_save(array(
        'xmpp_support_chat_freq' => variable_get('xmpp_support_chat_request_frequency', 3000),
      )
    );

    $this->xmppClient->add_cb('on_auth_failure', array($this, 'funOnAuthFailure'));
    $this->xmppClient->add_cb('on_auth_success', array($this, 'funOnAuthSuccessSendRawData'));
    $this->xmppClient->start();

    return $this->opStatus;
  }

  /**
   * Event function for status (Presence) message.
   *
   * @param object $stanza
   *   Incomming data from XMPP server. More see to
   *   http://xmpp.org/rfcs/rfc3920.html#stanzas.
   */
  public function funOnPresenceStanza($stanza) {
    if ($this->opStatus == FALSE) {
      $this->opStatus = ($stanza->type ? FALSE : TRUE);
    }
    $this->xmppClient->send_end_stream();
    return "logged_out";
  }

  /**
   * Constructor for class XmppSupportChatMessenger.
   *
   * @param array $init_params
   *   array of parameters for XMPP server.
   */
  public function __construct(array $init_params) {
    $this->params = $init_params;
    libraries_load("JAXL");
    $this->xmppClient = new JAXL($this->params);
  }

  /**
   * Event function for interrupt after timeout.
   */
  public function funTimerTimeout() {
    $this->xmppClient->send_end_stream();
    return "logged_out";
  }

  /**
   * Event timer for reduce CPU load.
   */
  public function funUsleep() {
    // Sleep 0.002 seconds.  Reduce CPU load.
    usleep(2000);
  }

  /**
   * Event when receive message.
   *
   * @param XMPPStanza $stanza
   *   Incoming data from XMPP server. More see to
   *   http://xmpp.org/rfcs/rfc3920.html#stanzas.
   */
  public function funOnMessage(XMPPStanza $stanza) {
    $this->respons = $this->respons .
              theme('xmpp_support_chat_seller_message',
                array(
                  'to_name' => $stanza->to_node,
                  'to_domain' => $stanza->to_domain,
                  'to_resource' => $stanza->to_resource,
                  'from_name' => $stanza->from_node,
                  'from_domain' => $stanza->from_domain,
                  'from_resource' => $stanza->from_resource,
                  'message' => $stanza->body,
                )
              );
  }

  /**
   * Event after success auth, sent present stanza to server.
   *
   * @return bool
   *   FALSE - event will disable else event stay enabled.
   */
  public function funOnAuthSuccessSendRawData() {
    $this->xmppClient->add_cb('on_chat_message', array($this, 'funOnMessage'));
    // Reduce CPU load.
    JAXLLoop::$clock->call_fun_periodic(-1, array($this, 'funUsleep'));
    JAXLLoop::$clock->call_fun_after(500 * pow(10, 3), array($this, 'funTimerTimeout'));
    $this->xmppClient->add_cb('on_presence_stanza', array($this, 'funOnPresenceStanza'));

    $this->xmppClient->send_raw($this->rawData);
    return FALSE;
  }

  /**
   * Event after success authentication in XMPP server.
   *
   * @return bool
   *   FALSE - event will disable else event stay enabled.
   */
  public function funOnAuthSuccess() {
    $this->xmppClient->set_status("available!");
    $this->xmppClient->add_cb('on_chat_message', array($this, 'funOnMessage'));
    // Reduce CPU load.
    JAXLLoop::$clock->call_fun_periodic(-1, array($this, 'funUsleep'));

    // Send current message.
    $this->xmppClient->send($this->msg);
    // Return formatted message for show it into chat field.
    if (strlen($this->msg->body) > 0) {
      $this->respons = $this->respons .
          theme('xmpp_support_chat_customer_message',
                array(
                  'message' => $this->msg->body,
                  'to_name' => variable_get('xmpp_support_chat_send_to', 'admin'),
                  'domain' => variable_get('xmpp_support_chat_server_domain', 'mysite'),
                  'name' => $_SESSION['xmpp_support_chat_user_name'],
                  'show_name' => $this->params['resource'],
                )
          );
    }
    // Set timeout for process.
    JAXLLoop::$clock->call_fun_after(500 * pow(10, 3), array($this, 'funTimerTimeout'));
    return TRUE;
  }

  /**
   * Event after wrong authentication in XMPP server.
   *
   * @return string
   *   "logged_out - event will disable else event stay enabled.
   */
  public function funOnAuthFailure() {
    unset($_SESSION['xmpp_support_chat_user_name']);
    $this->auth = FALSE;
    $this->xmppClient->send_end_stream();
    return "logged_out";
  }

  /**
   * Refresh conversation.
   *
   * @return string
   *   Rendered message.
   */
  public function refresh() {
    if (!isset($_SESSION['xmpp_suport_chat_sentence'])) {
      return "";
    }
    $t_last = $_POST['change'];
    XmppSupportChatMessage::$respons = "";
    foreach ($_SESSION['xmpp_suport_chat_sentence'] as $t => $value) {
      // If message older then 6 hours do remove it.
      if ($t < (time() - 18000)) {
        unset($_SESSION['xmpp_suport_chat_sentence'][$t]);
      }
      else {
        if ($t_last < $t) {
          $this->result[$t] = $value;
        }
      }
    }
    return json_encode($this->result);
  }

  /**
   * Function send current text message and check received messages.
   *
   * @param string $to
   *   Address of receiver for the message.
   * @param string $text
   *   Text of message for sending.
   *
   * @return string
   *   json - with array of last messages.
   */
  public function request($to, $text) {
    libraries_load("JAXL");
    if (session_status() === PHP_SESSION_NONE) {
      session_start();
    }
    if ((!isset($_SESSION['xmpp_support_chat_user_name'])) ||
         strlen($_SESSION['xmpp_support_chat_user_name']) < 1) {
      return "";
    }

    // Are checking how often a client send request to server.
    // Default limit one request for 3 seconds (3000/1000 milliseconds).
    $freq = round(variable_get('xmpp_support_chat_request_frequency', 3000) / 1000);
    if ($freq == 0) {
      $freq == 1;
    }

    if (isset($_SESSION['xmpp_support_chat_user_tik'])) {
      if (((time() - $_SESSION['xmpp_support_chat_user_tik']) < $freq) && (strlen($text) == 0)) {
        return $this->refresh();
      }
    }
    $this->msg = new XMPPMsg(
      array(
        'type' => 'chat',
        'to' => $to,
        'from' => $this->params['jid'],
      ),
      $text,
      '',
      ''
    );

    $_SESSION['xmpp_support_chat_user_tik'] = time();
    session_write_close();

    $this->xmppClient->add_cb('on_auth_failure', array($this, 'funOnAuthFailure'));
    $this->xmppClient->add_cb('on_auth_success', array($this, 'funOnAuthSuccess'));

    $this->xmppClient->start();
    if (session_status() === PHP_SESSION_NONE) {
      session_start();
    }

    if (strlen($this->respons) > 2) {
      setcookie('xmpp_support_chat_message_status', 'chat', time() + 18000, '/');
      $t = time();
      $_SESSION['xmpp_suport_chat_sentence'][$t] = $this->respons;
      $this->result[$t] = $this->respons;
      setcookie('xmpp_support_chat_message_sleep', '3', $freq, '/');
      return json_encode($this->result);
    }
    else {
      setcookie('xmpp_support_chat_message_sleep', '3', $freq, '/');
      return "";
    }
  }

}
